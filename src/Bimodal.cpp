/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

/* 
 * Definition of the bimodal prediction algorithm   
*/
void Bimodal
(
    char         Address [],
    char         Taken_NotTaken,
    unsigned int LastBits,
    Counter      *COUNTER,
    Arguments    *UserArgs,
    Results      *Result
)
{
    /* Index of the BHT */
    unsigned int Index;
  
    Index  = atoi( Address ) & LastBits; 

    /* Go through the BHT */            
    for( int i=0; i<UserArgs->BTH; i++ )
    {
        if( COUNTER[i].tag == Index )
        {

            if( COUNTER[i].State == "SNT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "correct"; 
                Result->Prediction_TN = 'N';
                Result->Correct_Not_Taken++;
            }
            else if( COUNTER[i].State == "WNT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "correct";  
                COUNTER[i].State      = "SNT";
                Result->Prediction_TN = 'N';
                Result->Correct_Not_Taken++;
            }
            else if( COUNTER[i].State == "SNT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WNT";
                Result->Prediction_TN = 'N';
                Result->Incorrect_Taken++;
            }
            else if( COUNTER[i].State == "WNT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "incorrect"; 
                COUNTER[i].State      = "WT" ;
                Result->Prediction_TN = 'N';
                Result->Incorrect_Taken++;
            }

            else if( COUNTER[i].State == "ST" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WT" ;
                Result->Prediction_TN = 'T';
                Result->Incorrect_Not_Taken++;
            }
            else if( COUNTER[i].State == "WT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WNT";
                Result->Prediction_TN = 'T';
                Result->Incorrect_Not_Taken++;
            }
            else if( COUNTER[i].State == "ST" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "correct"; 
                Result->Prediction_TN = 'T';
                Result->Correct_Taken++;
            }
            else if( COUNTER[i].State == "WT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "correct"; 
                COUNTER[i].State      = "ST";
                Result->Prediction_TN = 'T';
                Result->Correct_Taken++;
            }                              

            i = UserArgs->BTH; /* Loop for end */

        } /* If end */

    } /* For end */  

} /* END of function */