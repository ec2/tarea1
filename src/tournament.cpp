/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

/*
 * Definition of the tournament predictor.
 */
void Tournament
(
    char          Address [],
    char          Taken_NotTaken,
    unsigned int  LastBits,
    MetaPredictor *Meta_Predictor,
    Arguments     *UserArgs,
    Results       *Result
)
{
    /* Index used for go throught the meta predictor */
    unsigned int Index;

    Index  = atoi( Address ) & LastBits;

    /* The state machine of the tournament predictor */
    if( Meta_Predictor[Index].State == "SPPS" )
    {
        if( Result->Correct_Global == "correct" && Result->Correct_Private == "incorrect" )
        {
            Meta_Predictor[Index].State = "WPPS";                    
        }/* if end */
                
        Result->Prediction_TN       = Result->Private_prediction;
        Result->Tournament_pred_sel = 'P';

    }/* if end */


    else if( Meta_Predictor[Index].State == "WPPS" )
    {
        if( Result->Correct_Global == "incorrect" && Result->Correct_Private == "correct" )
        {
            Meta_Predictor[Index].State = "SPPS";
        }/* if end */
        else if( Result->Correct_Global == "correct" && Result->Correct_Private == "incorrect" )
        {
            Meta_Predictor[Index].State = "WPG";
        }/* else if end */
                                
        Result->Prediction_TN       = Result->Private_prediction;
        Result->Tournament_pred_sel = 'P';

    }/* else if end */


    else if( Meta_Predictor[Index].State == "WPG" )
    {
        if( Result->Correct_Global == "incorrect" && Result->Correct_Private == "correct" )
        {
            Meta_Predictor[Index].State = "WPPS";
        }/* if end */
        else if( Result->Correct_Global == "correct" && Result->Correct_Private == "incorrect" )
        {
            Meta_Predictor[Index].State = "SPG";
        }/* else if end */
               
        Result->Prediction_TN        = Result->Global_Prediction;
        Result->Tournament_pred_sel = 'G';

    }/* else if end */


    else if( Meta_Predictor[Index].State == "SPG" )
    {
        if( Result->Correct_Global == "incorrect" && Result->Correct_Private == "correct" )
        {
            Meta_Predictor[Index].State = "WPG";                    
        }/* if end */
                
        Result->Prediction_TN       = Result->Global_Prediction;
        Result->Tournament_pred_sel = 'G';

    }/* else if end */


    if ( Taken_NotTaken == 'T' )
    {
        if( Result->Prediction_TN == 'T' ) 
        {
            Result->Tour_Correct_Taken++;
            Result->Correct = "correct";
        }
        else
        {
            Result->Tour_Incorrect_Taken++;
            Result->Correct = "incorrect";            
        }
        
    }/* if end */
    else
    {
        if( Result->Prediction_TN == 'T' ) 
        {
            Result->Tour_Incorrect_Not_Taken++;
            Result->Correct = "incorrect";
        }
        else
        {
            Result->Tour_Correct_Not_Taken++;
            Result->Correct = "correct";            
        }
    }/* else end */
            

}/* End of function */