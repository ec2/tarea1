/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#include <iostream>
#include <string.h>
#include <math.h>
#include <ctime>
#include "../includes/In_Out_Manager.h"
#include "../includes/Bimodal.h"
#include "../includes/global.h"
#include "../includes/privada.h"
#include "../includes/tournament.h"

using namespace std;

/* 
 * Constants that indicate if the function finished correctly.
*/
enum PROGRAM_CORRECT_EXECUTION
{ 
  EXECUTION_OK    = 0,
  EXECUTION_ERROR = 1
};/* enum end */


int main( int argc, char** argv )
{
  /* Args and structs declarations */
  Arguments UserArgs;
  Results   Result;
  
  /* Struct timespec and double time are arguments by default for the clock */
  struct timespec t0, t1; 
  double time;

  char         Address [10];
  char         Taken_NotTaken;
  unsigned int Index;
  char         Prediction_TN;
  string       Correct;
  int          Decimal_GH; /* Decimal value of GHR */
  unsigned int LastBits;

  /* Global history register */
  vector<int> GH;

  /* Verify if the arguments are correct */
  if( ParseArguments( argc, argv, &UserArgs ) == false )
  {
    return EXECUTION_ERROR;
  }/* If end */


  /* Table that stores the branch history */
  Jump_History JUMP_HISTORY[UserArgs.BTH];

  /* Declaration of the meta predictor */
  MetaPredictor Meta_Predictor[UserArgs.BTH];


  /* Create the archive based on the selected predictor */
  if( UserArgs._O == 1 )
  {
    Create_archive( UserArgs.PREDICTOR );
  }/* If end */

  /* Predictor register array */
  Counter COUNTER[UserArgs.BTH];
  /* Predictor register array used in Tournament */
  Counter COUNTER_TOUR[UserArgs.BTH];

  /* Mask, number of bits of the GHR */
  LastBits = UserArgs.BTH - 1;

  /* Predictor register power up */  
  for( int i=0; i<UserArgs.BTH; i++)
  {
      COUNTER[i].tag      = i;
      COUNTER_TOUR[i].tag = i;
  } /* For end */


  /* Global history register power up to Not taken */
  for( int i=0; i<UserArgs.GH; i++)
  {             
      GH.push_back(0);
  }/* for end */


  /* Branch history register power up */  
  for( int i=0; i<UserArgs.BTH; i++)
  {
      for( int j=0; j<UserArgs.PH; j++ )
      {
        JUMP_HISTORY[i].GH.push_back(0);
      }/* for end */

  } /* For end */

  /* Initialze the clock */
  clock_gettime( CLOCK_MONOTONIC, &t0 );

  /* Call the apropiate algorithm based on the selected predictor */
  if ( UserArgs.PREDICTOR == "bimodal" )
  {
      while( scanf("%s %c\n", &Address, &Taken_NotTaken) != EOF )
      {
        Result.Number_of_branch++;

        Bimodal( Address, Taken_NotTaken, LastBits, COUNTER, &UserArgs, &Result );

        if ( UserArgs._O == 1 )
          print_predictor( UserArgs.PREDICTOR, Address, Taken_NotTaken, &Result );  

      }/* while end */
    
  }/* if end */
  
  
  else if( UserArgs.PREDICTOR == "global"  )
  { 
      while( scanf("%s %c\n", &Address, &Taken_NotTaken) != EOF )
      {
        Result.Number_of_branch++;
        
        Global ( Address, Taken_NotTaken, LastBits, COUNTER, &GH, &UserArgs, &Result );
        
        if ( UserArgs._O == 1 )
          print_predictor( UserArgs.PREDICTOR, Address, Taken_NotTaken, &Result );      
      
      }/* while end*/

  }/* else if end */


  else if( UserArgs.PREDICTOR == "privada" ) 
  {
      while( scanf("%s %c\n", &Address, &Taken_NotTaken) != EOF )
      {
        Result.Number_of_branch++;

        Privada( Address, Taken_NotTaken, LastBits, COUNTER, JUMP_HISTORY, &UserArgs, &Result );

        if ( UserArgs._O == 1 )
          print_predictor( UserArgs.PREDICTOR, Address, Taken_NotTaken, &Result );  

      }/* while end*/

  }/* else if end */


  else if( UserArgs.PREDICTOR == "torneo"  )
  {   
      while( scanf("%s %c\n", &Address, &Taken_NotTaken) != EOF )
      {
        Result.Number_of_branch++;
    
        Global (Address, Taken_NotTaken, LastBits, COUNTER, &GH, &UserArgs, &Result); 
        Result.Global_Prediction = Result.Prediction_TN;
        Result.Correct_Global    = Result.Correct; 

        Privada(Address, Taken_NotTaken, LastBits, COUNTER_TOUR, JUMP_HISTORY, &UserArgs, &Result); 
        Result.Private_prediction = Result.Prediction_TN;
        Result.Correct_Private    = Result.Correct;

        Tournament ( Address, Taken_NotTaken, LastBits, Meta_Predictor, &UserArgs, &Result );

        if ( UserArgs._O == 1 )
          print_predictor( UserArgs.PREDICTOR, Address, Taken_NotTaken, &Result );        
     
      }/* while end */

      Result.Correct_Taken       = Result.Tour_Correct_Taken;
      Result.Incorrect_Taken     = Result.Tour_Incorrect_Taken;
      Result.Correct_Not_Taken   = Result.Tour_Correct_Not_Taken;
      Result.Incorrect_Not_Taken = Result.Tour_Incorrect_Not_Taken;

  }/* else if end */

  else return EXECUTION_ERROR;

  /* End the clock */
  clock_gettime( CLOCK_MONOTONIC, &t1 );


  /* Print on console the run stats */
  print_stats( &UserArgs, &Result );


  /* Elapsed time for execute the algorithms */
  time =  (t1.tv_sec - t0.tv_sec);
  time += (t1.tv_nsec - t0.tv_nsec)/1000000000.0;
  cout << "Execution time: " << time << " seconds";
  cout << endl;


  /* The program end without any problem */
  cout << "End of program" << endl << endl;
  return EXECUTION_OK;
}
/* 
 * End of main
*/