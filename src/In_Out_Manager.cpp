/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

/*
 * Check if the parameters passed by the terminal are correct, 
 * then store them in variables to be used in the program.   
*/
bool ParseArguments
( 
    int       argc, 
    char**    argv,
    Arguments *UserArgs
)
{  
  bool _arg_error = false;

  if(argc == 11) /*User NEED to pass 10 arguments*/
  {
    if( strcmp("-s", argv[1]) == 0 )
    {
      if( isdigit( *argv[2] ) != 0 ) UserArgs->BTH = pow( 2.0, atoi(argv[2]) ); 
      else _arg_error = true;
    }/* if end */
    else _arg_error = true;
  

    if( strcmp( "-bp", argv[3]) == 0 )
    {
      if     ( isdigit(*argv[4]) != 0 && atoi(argv[4])==0 ) UserArgs->PREDICTOR = "bimodal";
      else if( isdigit(*argv[4]) != 0 && atoi(argv[4])==1 ) UserArgs->PREDICTOR = "global";
      else if( isdigit(*argv[4]) != 0 && atoi(argv[4])==2 ) UserArgs->PREDICTOR = "privada";
      else if( isdigit(*argv[4]) != 0 && atoi(argv[4])==3 ) UserArgs->PREDICTOR = "torneo";
      else _arg_error = true;
    }/* if end */
    else _arg_error = true;
    

    if( strcmp("-gh", argv[5]) == 0 )
    {
      if( isdigit( *argv[6]) != 0 ) UserArgs->GH = atoi(argv[6]);
      else _arg_error = true;
      
    }/* if end */
    else _arg_error = true;
    
  
    if( strcmp("-ph", argv[7]) == 0 ) /* Protocol */
    {
      if( isdigit( *argv[8]) != 0 ) UserArgs->PH = atoi(argv[8]);
      else _arg_error = true; 
    }/* if end */ 


    if( strcmp("-o", argv[9]) == 0 ) /* Protocol */
    {
      if( isdigit( *argv[10]) != 0 ) UserArgs->_O = atoi(argv[10]);
      else _arg_error = true;   
    }/* if end */ 
    else _arg_error = true;
  }

  else _arg_error = true;
      

  if(_arg_error == true)
  {
    cout << endl;
    cout << "ERROR verify the arguments...";
    cout << endl << endl;
    return false;
  }/* if end */
  else return true;

};/* END OF FUNCTION */


/* 
 * Show in the terminal the results of the program.   
*/
void print_stats 
(
  Arguments *UserArgs,
  Results   *Result
)
{
  float CorrPredPer;
  CorrPredPer = ( static_cast<double>( Result->Correct_Taken + Result->Correct_Not_Taken ) / Result->Number_of_branch)*100;

  cout << endl << "______________________________________________________________"       << endl << endl;
  cout << "  Prediction parameters:"                                                     << endl;
  cout << "______________________________________________________________"               << endl << endl;
  cout << "  Branch prediction type:                            " << UserArgs->PREDICTOR << endl;
  cout << "  BHT size (entries):                                " << UserArgs->BTH       << endl;
  cout << "  Global history register size:                      " << UserArgs->GH        << endl;
  cout << "  Private history register size:                     " << UserArgs->PH        << endl;
  cout << "______________________________________________________________"               << endl << endl;
  cout << "  Simulation results:             "                                           << endl;
  cout << "______________________________________________________________"               << endl << endl; 
  cout << "  Number of branch:                                  " << Result->Number_of_branch    << endl;
  cout << "  Number of correct prediction of taken branches:    " << Result->Correct_Taken       << endl;
  cout << "  Number of incorrect prediction of taken branches:  " << Result->Incorrect_Taken     << endl;
  cout << "  Correct Prediction of not taken branches:          " << Result->Correct_Not_Taken   << endl;
  cout << "  Incorrect prediction of not taken branches:        " << Result->Incorrect_Not_Taken << endl;
  cout << "  Percentage of correct predictions:                 " << CorrPredPer                 << endl;
  cout << "______________________________________________________________" << endl << endl << endl;
};/* END OF FUNCTION */


/*
 * Create an archive TXT, the name depend on the selected predictor.
*/
void Create_archive
(
  string PREDICTOR   
)
{
  ofstream FILE;

  if     ( PREDICTOR == "bimodal" ) 
  {
    FILE.open("./results/bimodal.txt");
    FILE <<"PC           Outcome  Prediction  correct/incorrect\n";
  }/* if end */
  else if( PREDICTOR == "global"  ) 
  {
    FILE.open("./results/global.txt");
    FILE <<"PC           Outcome  Prediction  correct/incorrect\n";
  }/* if end */

  else if( PREDICTOR == "privada" ) 
  {
    FILE.open("./results/privada.txt");
    FILE <<"PC           Outcome  Prediction  correct/incorrect\n";
  }/* else if end */

  else if( PREDICTOR == "torneo"  ) 
  {
    FILE.open("./results/torneo.txt");
    FILE <<"PC           Predictor Outcome  Prediction  correct/incorrect\n";
  }/* else if end */

};/* END OF FUNCTION */


/* 
 * Write on the created archive a new line of data.
*/
void print_predictor
(
    string  PREDICTOR,
    char*   Address,
    char    Taken_Not_taken,
    Results *Result
)
{
  ofstream out;

  if     ( PREDICTOR == "bimodal" )
  {
    out.open("./results/bimodal.txt", ios::app);

    out << Address << "   " << Taken_Not_taken; 
    out << "        "       << Result->Prediction_TN;
    out << "           "    << Result->Correct << endl;  
  }/* if end */

  else if( PREDICTOR == "global"  ) 
  {
    out.open("./results/global.txt" , ios::app);

    out << Address << "   " << Taken_Not_taken; 
    out << "        "       << Result->Prediction_TN;
    out << "           "    << Result->Correct << endl; 
  }/* else if end */

  else if( PREDICTOR == "privada" ) 
  {
    out.open("./results/privada.txt", ios::app);

    out << Address << "   " << Taken_Not_taken; 
    out << "        "       << Result->Prediction_TN;
    out << "           "    << Result->Correct << endl; 
  }/* else if end */

  else if( PREDICTOR == "torneo"  )
  { 
    out.open("./results/torneo.txt" , ios::app);
  
    out << Address << "   " << Result->Tournament_pred_sel;
    out << "         " << Taken_Not_taken;
    out << "        "       << Result->Prediction_TN;
    out << "           "    << Result->Correct << endl;   
  }/* else if end */

};/* END OF FUNCTION */