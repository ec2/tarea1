/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

/* Definition of the private history predictor */
void Privada
(
    char         Address [],
    char         Taken_NotTaken,
    unsigned int LastBits,
    Counter      *COUNTER,
    Jump_History *JUMP_HISTORY,
    Arguments    *UserArgs,
    Results      *Result
)
{
    /* Index for the jump history register and the BHT */
    unsigned int Index;
    int          Decimal_GH; /* Decimal value of GHR */
  
    Decimal_GH = 0; /* Reinitialize the decimal GH */

    /* Index for find the branch history register */
    Index = atoi( Address ) & LastBits;


    /* GHR to DEC */
    for( int i=0; i < UserArgs->PH; i++)
    {
        Decimal_GH += JUMP_HISTORY[Index].GH[i]*pow( 2, i);
    }/* for end */

    
    /* Update de GHR */       
    /* Rotate right the register */
    rotate( 
        JUMP_HISTORY[Index].GH.begin(), 
        JUMP_HISTORY[Index].GH.begin() + JUMP_HISTORY[Index].GH.size()-1, 
        JUMP_HISTORY[Index].GH.end() );
    
            
    /* Insert 0 or 1 in the first position in the GHR */
    if     ( Taken_NotTaken == 'T' ) JUMP_HISTORY[Index].GH.at( 0 ) = 1;
    else if( Taken_NotTaken == 'N' ) JUMP_HISTORY[Index].GH.at( 0 ) = 0;
    

    Index = ( (Decimal_GH) ^ (atoi( Address )&LastBits) );

    for( int i=0; i<UserArgs->BTH; i++ )
    {
        if( COUNTER[i].tag == Index )
        {

            if( COUNTER[i].State == "SNT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "correct"; 
                Result->Prediction_TN = 'N';
                Result->Correct_Not_Taken++;
            }
            else if( COUNTER[i].State == "WNT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "correct";  
                COUNTER[i].State      = "SNT";
                Result->Prediction_TN = 'N';
                Result->Correct_Not_Taken++;
            }
            else if( COUNTER[i].State == "SNT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WNT";
                Result->Prediction_TN = 'N';
                Result->Incorrect_Taken++;
            }
            else if( COUNTER[i].State == "WNT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "incorrect"; 
                COUNTER[i].State      = "WT" ;
                Result->Prediction_TN = 'N';
                Result->Incorrect_Taken++;
            }

            else if( COUNTER[i].State == "ST" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WT" ;
                Result->Prediction_TN = 'T';
                Result->Incorrect_Not_Taken++;
            }
            else if( COUNTER[i].State == "WT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WNT";
                Result->Prediction_TN = 'T';
                Result->Incorrect_Not_Taken++;
            }
            else if( COUNTER[i].State == "ST" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "correct"  ; 
                Result->Prediction_TN = 'T';
                Result->Correct_Taken++;
            }
            else if( COUNTER[i].State == "WT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "correct"  ; 
                COUNTER[i].State      = "ST";
                Result->Prediction_TN = 'T';
                Result->Correct_Taken++;
            }
               

            i = UserArgs->BTH; /* Loop for end */

        } /* If end */

    } /* For end */

}/* End of function */