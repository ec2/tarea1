/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

/*
 * Definition of the predictor with global history.
 */
void Global
(
    char         Address [],
    char         Taken_NotTaken,
    unsigned int LastBits,
    Counter      *COUNTER,
    vector<int>*  GH,
    Arguments    *UserArgs,
    Results      *Result
)
{
    /* Index used for go throught the BHT */
    unsigned int Index;
    int          Decimal_GH = 0; /* Decimal value of GHR */
 

    /* GHR from binary to DEC */
    for( int i=0; i < UserArgs->GH; i++)
    {
        Decimal_GH += GH->at(i)*pow( 2, i);
    }/* for end */


    /* Update de GHR */       
    /* Rotate right the register */
    rotate( GH->begin(), GH->begin()+GH->size()-1, GH->end() );
    
            
    /* Insert 0 or 1 in the first position in the GHR */
    if     ( Taken_NotTaken == 'T' ) GH->at( 0 ) = 1;
    else if( Taken_NotTaken == 'N' ) GH->at( 0 ) = 0;


    /* Calculate the index for the BHT */
    Index =  ((Decimal_GH)&LastBits) ^ (atoi( Address )&LastBits) ;
        
    /* Go through the BHT */  
    for( int i=0; i<UserArgs->BTH; i++ )
    {
        if( COUNTER[i].tag == Index )
        {

            if( COUNTER[i].State == "SNT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "correct"; 
                Result->Prediction_TN = 'N';
                Result->Correct_Not_Taken++;
            }
            else if( COUNTER[i].State == "WNT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct       = "correct";  
                COUNTER[i].State      = "SNT";
                Result->Prediction_TN = 'N';
                Result->Correct_Not_Taken++;
            }
            else if( COUNTER[i].State == "SNT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct       = "incorrect";  
                COUNTER[i].State      = "WNT";
                Result->Prediction_TN = 'N';
                Result->Incorrect_Taken++;
            }
            else if( COUNTER[i].State == "WNT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct = "incorrect"; 
                Result->Incorrect_Taken++;
                COUNTER[i].State = "WT" ;
                Result->Prediction_TN = 'N';
            }

            else if( COUNTER[i].State == "ST" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct = "incorrect";
                Result->Incorrect_Not_Taken++;  
                COUNTER[i].State = "WT" ;
                Result->Prediction_TN = 'T';
            }
            else if( COUNTER[i].State == "WT" && Taken_NotTaken == 'N' ) 
            {
                Result->Correct = "incorrect"; 
                Result->Incorrect_Not_Taken++; 
                COUNTER[i].State = "WNT";
                Result->Prediction_TN = 'T';
            }
            else if( COUNTER[i].State == "ST" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct = "correct"  ; 
                Result->Correct_Taken++;
                Result->Prediction_TN = 'T';
            }
            else if( COUNTER[i].State == "WT" && Taken_NotTaken == 'T' ) 
            {
                Result->Correct = "correct"  ;
                Result->Correct_Taken++; 
                COUNTER[i].State  = "ST";
                Result->Prediction_TN = 'T';
            }                

            i = UserArgs->BTH; /* Loop for end */

        } /* If end */

    } /* For end */
  

}/* End of function */