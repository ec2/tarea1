# Branch predictor simulator IE521 project
Code base line for IE0521 cache simulation project
Created by Jorge Munoz Taylor

## How to build the project
In directory "tarea1":
```
>> make

```

## How to run the simulation
The simulation executable is located inside the main directory (proyecto_parte1):
```
>> gunzip -c branch-trace-gcc.trace.gz | head -<#> | ./branch -s <#> -bp <#> -gh <#> -ph <#> -o <#> 
```
The -s only accept the parameters: 0 (bimodal), 1 (global history), 2 (private history), 3 (tournament)

The -o accept 1 for create the document, an any other value for only show the results on the terminal.

## Results
```
The simulation results are shown on the directory "results" if the opcion -o was 1.