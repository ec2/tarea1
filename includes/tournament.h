/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#ifndef _TOURNAMENT_H_
#define _TOURNAMENT_H_

#include <iostream>
#include <string.h>
#include <math.h>
#include <bits/stdc++.h>
#include <vector>
#include <algorithm> /* Rotate function */
#include "In_Out_Manager.h"
#include "privada.h"
#include "global.h"

using namespace std;

/*
 * One cell of the MetaPredictor, contain the prediction state of one branch.
 */
struct MetaPredictor
{
    string  State = "SPPS"; /* Strongly prefer pshared */
};


/*
 * Tournament predictor algorithm.
 * 
 * [in]     Address: Branch address in decimal.
 * [in]     Taken_NotTaken: If the branch has been taken or not.
 * [in]     LastBits: Bits number of the size of the BHT.
 * [in/out] Meta_Predictor: Meta predictor register.
 * [in]     UserArgs: Parameter introduced by the user
 * [out]    Result: Struct that stores the data that will be printed on the terminal.
 */
void Tournament
(
    char          Address [],
    char          Taken_NotTaken,
    unsigned int  LastBits,
    MetaPredictor *Meta_Predictor,
    Arguments     *UserArgs,
    Results       *Result
);

#include "../src/tournament.cpp"

#endif