/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#ifndef _PRIVADA_H_
#define _PRIVADA_H_

#include <iostream>
#include <string.h>
#include <math.h>
#include <bits/stdc++.h>
#include <vector>
#include <algorithm> /* Rotate function */
#include "In_Out_Manager.h"

using namespace std;


/* Store the jump history of a branch */
struct Jump_History
{
    /* Global history register */
    vector<int> GH;
};

/*
 * Private history predictor algorithm.
 * 
 * [in]     Address: Branch address in decimal.
 * [in]     Taken_NotTaken: If the branch has been taken or not.
 * [in]     LastBits: Bits number of the size of the BHT.
 * [in/out] COUNTER: BHT single cell.
 * [in/out] JUMP_HISTORY: Entire history of the branchs.
 * [in]     UserArgs: Parameter introduced by the user
 * [out]    Result: Struct that stores the data that will be printed on the terminal.
 */
void Privada
(
    char         Address [],
    char         Taken_NotTaken,
    unsigned int LastBits,
    Counter      *COUNTER,
    Jump_History *JUMP_HISTORY,
    Arguments    *UserArgs,
    Results      *Result
);

#include "../src/privada.cpp"

#endif