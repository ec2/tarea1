/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <iostream>
#include <string.h>
#include <math.h>
#include <bits/stdc++.h>
#include <algorithm> /* rotate function */
#include <vector>
#include "In_Out_Manager.h"

using namespace std;

/*
 * Global predictor algorithm.
 * 
 * [in]     Address: Branch address in decimal.
 * [in]     Taken_NotTaken: If the branch has been taken or not.
 * [in]     LastBits: Bits number of the size of the BHT.
 * [in/out] COUNTER: BHT single cell.
 * [in/out] GH: History of the last branchs, is an array of 1s/0s.
 * [in]     UserArgs: Parameter introduced by the user
 * [out]    Result: Struct that stores the data that will be printed on the terminal.
 */
void Global
(
    char         Address [],
    char         Taken_NotTaken,
    unsigned int LastBits,
    Counter      *COUNTER,
    vector<int>  *GH,
    Arguments    *UserArgs,
    Results      *Result
);

#include "../src/global.cpp"

#endif