/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#ifndef _IN_OUT_MANAGER_
#define _IN_OUT_MANAGER_

#include <iostream>
#include <string.h>
#include <fstream>

using namespace std;

/*
 * Struct that store the user inputs.
 */
struct Arguments
{
  int     BTH;
  string  PREDICTOR;
  int     GH;
  int     PH;
  uint8_t _O;
};


/*
 * One cell counter, contain the prediction state and the tag of the cell.
 */
struct Counter
{
    string  State = "SNT";
    int     tag;
};


/*
 * Struct that stores the data that will be printed on the terminal.
 */
struct Results
{
    unsigned int Number_of_branch    = 0;
    
    unsigned int Correct_Taken       = 0;
    unsigned int Incorrect_Taken     = 0;
    unsigned int Correct_Not_Taken   = 0;
    unsigned int Incorrect_Not_Taken = 0; 

    unsigned int Tour_Correct_Taken       = 0;
    unsigned int Tour_Incorrect_Taken     = 0;
    unsigned int Tour_Correct_Not_Taken   = 0;
    unsigned int Tour_Incorrect_Not_Taken = 0; 

    char         Global_Prediction;
    string       Correct_Global;
    char         Private_prediction;
    string       Correct_Private;    
    
    char         Prediction_TN;
    string       Correct;
    char         Tournament_pred_sel;
};


/*
 * Check if the parameters passed by the terminal are correct, 
 * then store them in variables to be used in the program.
 * 
 * [in] argc: Number of parameters that were introduced.
 * [in] argv: The data of the introduced parameters.
 * [out] UserArgs: Struct that receive the parameters. 
 */
bool ParseArguments
( 
    int       argc, 
    char**    argv,
    Arguments *UserArgs
);


/*
 * Create the archive TXT in the directory called -> results <-.
 * 
 * [in] PREDICTOR: Predictor selected by the user.
 */
void Create_archive
(
   string PREDICTOR 
);


/*
 * Shows in the terminal the result of the algorithm. 
 * 
 * [in]  UserArgs: Struct that contain the user parameters.
 * [out] Result: Struct that stores the correct/incorrect counters.
 */
void print_stats 
(
    Arguments *UserArgs,
    Results   *Result
);


/*
 * Write on the archive the stats of the program.
 * 
 * [in] PREDICTOR: Predictor selected by the user.
 * [in] Address: Branch address.
 * [in] Taken_Not_Taken: If the branch is taken or not.
 * [in] Correct: If the prediction is correct.
 */
void print_predictor
(
    string  PREDICTOR,
    char*   Address,
    char    Taken_Not_taken,
    Results *Result
);

#include "../src/In_Out_Manager.cpp"

#endif