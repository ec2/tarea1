/*
 *  University of Costa Rica
 *  Jorge Munoz Taylor
 *  Cache branch predictor
 *  Class UCR IE-521
 *  Semester: II-2019
*/

#ifndef _Bimodal_H_
#define _Bimodal_H_

#include <iostream>
#include <string.h>
#include <math.h>
#include <bits/stdc++.h>
#include "In_Out_Manager.h" /* For struct Arguments */ 

using namespace std;

/*
 * Bimodal predictor algorithm.
 * 
 * [in]     Address: Branch address in decimal.
 * [in]     Taken_NotTaken: If the branch has been taken or not.
 * [in]     LastBits: Bits number of the size of the BHT.
 * [in/out] COUNTER: BHT single cell.
 * [in]     UserArgs: Parameter introduced by the user
 * [out]    Result: Struct that stores the data that will be printed on the terminal.
 */
void Bimodal
(
    char         Address [],
    char         Taken_NotTaken,
    unsigned int LastBits,
    Counter      *COUNTER,
    Arguments    *UserArgs,
    Results      *Result
);


#include "../src/Bimodal.cpp"

#endif